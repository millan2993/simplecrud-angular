export class IUser {
    _id?: string;
    name: string;
    email: string;
}
