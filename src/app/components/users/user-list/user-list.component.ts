import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {IUser} from '../IUser';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {filter} from 'rxjs/operators';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

    public users: Observable<IUser[]>;

    public userSelected: IUser;

    constructor(private userService: UserService,
                private router: Router) {
    }

    ngOnInit() {
        this.users = this.userService.users$;
    }


    showUser(user: IUser) {
        this.router.navigate(['users', 'show', user._id]);
    }


    selected(user: IUser) {
        this.userSelected = user;
    }


    async save() {
        const name = (<HTMLInputElement> document.getElementById('name')).value;
        const email = (<HTMLInputElement> document.getElementById('email')).value;

        const userSave: IUser = {
            name,
            email
        };

        if (typeof this.userSelected !== 'undefined') {
            userSave._id = this.userSelected._id;

            await this.userService.edit(userSave)
                .then(() => {
                    this.userSelected.name = name;
                    this.userSelected.email = email;

                }).catch((err) => {
                    console.log(err);
                    console.log('Ocurrió un error al guardar el registro');
                });

        } else {

            await this.userService.create(userSave)
                .then(() => {
                    console.log('usuario guardado');

                }).catch((err) => {
                    console.log(err);
                    console.log('Ocurrió un error al guardar el registro');
                });
        }

        this.userSelected = undefined;
    }



    clear() {
        this.userSelected = undefined;

        (<HTMLInputElement> document.getElementById('name')).value = null;
        (<HTMLInputElement> document.getElementById('email')).value = null;
    }


    async delete(user: IUser) {

        await this.userService.delete(user._id)
            .then()
            .catch();

        this.clear();
    }

}
