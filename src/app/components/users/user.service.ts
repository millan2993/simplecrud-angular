import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';
import {IUser} from './IUser';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private path = 'http://localhost:3000/users';

    private users = new BehaviorSubject<IUser[]>([]);
    public users$ = this.users.asObservable();

    constructor(private http: HttpClient) {
        this.loadUsers();
    }

    loadUsers() {
        this.http.get(this.path).subscribe((users: IUser[]) => {
            console.log(users);
            this.users.next(users);
        });
    }


    edit(user: IUser): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const path = this.path + '/' + user._id;

            this.http.put(path, user).subscribe(() => {
                resolve(true);
            }, error1 => {
                reject(false);
            });
        });
    }


    create(user: IUser): Promise<boolean> {
        return new Promise((resolve, reject) => {

            this.http.post(this.path, user).subscribe((userSaved: IUser) => {

                const currentUsers = this.users.value;
                const updatedUsers = [...currentUsers, userSaved];
                this.users.next(updatedUsers);

                resolve(true);

            }, error1 => {
                reject(false);
            });

        });
    }


    delete(id: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const path = this.path + '/' + id;

            this.http.delete(path).subscribe(() => {

                const usersUpdated = this.users.value.filter(user => user._id !== id);
                this.users.next(usersUpdated);
                resolve(true);

            }, error => {
                reject(false);
            });

        });
    }

}
