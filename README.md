# Simple CRUD

Este proyecto fue creado con [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8. 
Es ejemplo sencillo sobre un CRUD que se conecta a una API. 
Se debe indicar el endpoint base de la API (backend) en la prodiedad path en el archivo src/app/components/users/user.service.ts.
 

## Instalación

Puedes clonar este proyecto ejecutando el siguiente comando por la consola:

`git clone git@gitlab.com:millan2993/simplecrud-angular.git`
 
 Una vez descargado debes ubicarte en la carpeta raíz por la consola y ejecutar el siguiente comando para que se instalen las dependencias.

`npm install` 

Ejecute `ng serve` y navegue a `http://localhost:4200/`. para ejecutar el servidor y poder interactual con él.
